## Non-blocking REST processor for input text messages.
####Easy scalable REST message processor with saving in MongoDB and sending to external service. 

1. Create MongoDB 'chatme'. All configuration you can find in application.properties file.
2. Add admin user: 
```javascript
db.createUser(
  {
    user: "admin",
    pwd: "123456",
    roles: [ { role: "dbAdmin", db: "chatme" } ]
  }
)
```
3. Clean and Build application: 
```
gradle clean
gradle bootJar
```
4. Run jar: 
```
java -jar build/libs/chat-me-test-0.0.1-SNAPSHOT.jar
```

5. Send PUT request to http://localhost:4441/api/message/put URL and check output.
```json
{
	"text": "Text",
	"chatId": 123,
	"userId": "qwerty"
	
}
```
