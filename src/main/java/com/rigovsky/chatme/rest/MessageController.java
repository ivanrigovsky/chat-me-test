package com.rigovsky.chatme.rest;

import com.rigovsky.chatme.model.MessageContext;
import com.rigovsky.chatme.model.MessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.rigovsky.chatme.model.MessageRequest;
import com.rigovsky.chatme.service.MessageQueueService;

import javax.servlet.http.HttpServletRequest;

/**
 * Messages endpoint.
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/message")
public class MessageController {

    @Autowired
    private MessageQueueService queueService;

    /**
     * Save message information. Non-blocking
     * @param messageRequest message body
     */
    @RequestMapping(value = "/put", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void putMessage(@RequestBody MessageRequest messageRequest, HttpServletRequest request) {
        log.info("Request is received from {}", request.getRemoteAddr());
        queueService.put(messageRequest, request.getRemoteAddr(), System.currentTimeMillis());
    }

    /**
     * Receive message, to test.
     * @param message message body
     */
    @RequestMapping(value = "/receive", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void receiveMessage(@RequestBody MessageDTO message) {
        log.info("Message received {}", message.getTimestamp());
    }


}
