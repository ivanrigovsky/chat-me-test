package com.rigovsky.chatme.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rigovsky.chatme.model.MessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHeaders;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.rigovsky.chatme.model.MessageContext;
import com.rigovsky.chatme.repository.MessageRepository;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

import java.net.URI;

/**
 * Message sending service.
 */
@Slf4j
@Service
public class MessageSenderService {

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private MessageRepository repository;

    /**
     * Send message and mark as delivered
     * @param message message to send.
     */
    public void sendAndProcess(MessageContext message) {
        if (message != null) {
            MessageDTO dto = new MessageDTO(message.getMessage(), message.getTimestamp());
            if (send(dto)) {
                message.setDelivered(true);
                repository.save(message);
            } else {
                log.error("Can't send message {}", message.getId());
            }
        }
    }

    /**
     * Sender
     * @param message body
     * @return true - if 200 code was returned, false - all other cases.
     */
    private boolean send(MessageDTO message) {
        try {
            URI uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("localhost")
                    .setPath("/api/message/receive")
                    .setPort(4441)
                    .build();

            ResponseEntity response = restTemplate.postForEntity(uri, message, MessageDTO.class);
            return response.getStatusCode().equals(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Remote service is unavailable {}", e.getMessage(), e);
            return false;
        }
    }

}
