package com.rigovsky.chatme.service;

import com.rigovsky.chatme.model.MessageRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import com.rigovsky.chatme.model.MessageContext;
import com.rigovsky.chatme.repository.MessageRepository;
import java.util.Iterator;
import java.util.List;

import static java.lang.String.format;

/**
 * Message queue processor: saving, processing.
 */
@Slf4j
@Service
public class MessageQueueService {

    @Autowired
    private MessageRepository repository;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private MessageSenderService messageSender;

    /**
     * Save message and process into the queue. Non-blocking.
     * @param messageRequest request
     * @param ip user remote IP
     * @param timestamp timestamp
     */
    public void put(MessageRequest messageRequest, String ip, long timestamp) {
        taskExecutor.submit(() -> {
            log.info("Processing in separate thread");
            MessageContext message = MessageContext.builder()
                    .message(messageRequest)
                    .ip(ip)
                    .timestamp(timestamp)
                    .build();
            repository.save(message);
        });
    }

    /**
     * Message queue processor.
     */
    @Scheduled(fixedDelayString = "100")
    public void process() {
        try {
            int poolSize = taskExecutor.getCorePoolSize();
            int activeThread = taskExecutor.getActiveCount();
            int available = (poolSize - activeThread) * 2;

            if (available == 0) {
                log.error("No any available threads. Extend number of available threads.");
                return;
            }

            List<MessageContext> messages = repository.findByDeliveredIsFalse(PageRequest.of(0, available, Sort.by("timestamp")));
            Iterator<MessageContext> iterator = messages.iterator();
            log.debug(format("Active threads %d/%d %d in queue ", activeThread, poolSize, available));
            while (available > 0 && iterator.hasNext()) {
                available -= tryExecuteTask(iterator.next()) ? 1 : 0;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private boolean tryExecuteTask(MessageContext message) {
        try {
            taskExecutor.submit(() -> {
                messageSender.sendAndProcess(message);
            });
            return true;
        } catch (Exception e) {
            log.error("Task was rejected", e);
        }
        return false;
    }
}
