package com.rigovsky.chatme.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
@EnableScheduling
@Import({CommonConfig.class})
public class ServiceConfig {

    /**
     * Common task executor configuration for server instance.
     */
    private int threadsSize = 10;
    private int threadsSizeMax = 100;

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor ex = new ThreadPoolTaskExecutor();
        ex.setCorePoolSize(threadsSize);
        ex.setMaxPoolSize(threadsSizeMax);
        ex.initialize();
        return ex;
    }

}
