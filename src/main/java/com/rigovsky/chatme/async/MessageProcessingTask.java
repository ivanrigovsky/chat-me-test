package com.rigovsky.chatme.async;

import com.rigovsky.chatme.model.MessageContext;

/**
 * Message saving processor.
 */
public class MessageProcessingTask implements Runnable {

    private MessageContext messageContext;

    public MessageProcessingTask(MessageContext messageContext) {
        this.messageContext = messageContext;
    }

    @Override
    public void run() {
        //... something
    }
}
