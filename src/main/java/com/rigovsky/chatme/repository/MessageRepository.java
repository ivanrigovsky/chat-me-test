package com.rigovsky.chatme.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.rigovsky.chatme.model.MessageContext;

import java.util.List;

/**
 * Message repository layer.
 */
@Repository
public interface MessageRepository extends MongoRepository<MessageContext, String> {

    List<MessageContext> findByDeliveredIsFalse(Pageable pageable);

}
