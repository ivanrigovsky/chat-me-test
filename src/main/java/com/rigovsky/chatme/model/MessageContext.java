package com.rigovsky.chatme.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Entity with message request text
 */
@Data
@Builder
@Document
public class MessageContext {

    @Id
    private String id;
    private MessageRequest message;
    private String ip;
    private long timestamp;
    private boolean delivered;

}
