package com.rigovsky.chatme.model;

import lombok.Data;

/**
 * Request model from clients.
 */
@Data
public class MessageRequest {

    private String userId;
    private long chatId;
    private String text;
}
